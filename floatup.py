#!/usr/bin/env python3
#
# Read a hosts.yml float inventory, and manage a VM group derived by it.
# This tool is meant to replace "vagrant up" in a CI pipeline.
#

import argparse
import base64
import json
import os
import re
import shlex
import subprocess
import yaml
import zlib


def parse_inventory(path, host_attrs):
    with open(path) as fd:
        inventory = yaml.safe_load(fd)

    hosts = [{'ip': v['ansible_host'], 'name': k}
             for k, v in inventory['hosts'].items()]
    for h in hosts:
        h.update(host_attrs)

    # We know that the network is a /24.
    net = re.sub(r'\.[0-9]+$', '.0/24', hosts[0]['ip'])
    return {
        'network': net,
        'hosts': hosts,
    }


def do_request(url, ssh_gw, payload):
    data = json.dumps(payload)
    cmd = "curl -s -X POST -H 'Content-Type: application/json' -d %s %s" % (
        shlex.quote(data), url)
    if ssh_gw:
        cmd = "ssh %s %s" % (ssh_gw, shlex.quote(cmd))

    output = subprocess.check_output(cmd, shell=True)
    try:
        return json.loads(output)
    except json.decoder.JSONDecodeError:
        print(f'server error: {output}')
        raise


def encode_dashboard_request(req):
    # JSON data, in a raw zlib stream, base64-encoded.
    payload = {
        'inv': sorted(req['hosts'], key=lambda x: x['name']),
        'job': os.getenv('CI_JOB_ID'),
        'proj': os.getenv('CI_PROJECT_PATH'),
    }
    data = json.dumps(payload, separators=(',', ':')).encode('utf-8')
    comp = zlib.compressobj(level=9, wbits=-9)
    comp.compress(data)
    return base64.urlsafe_b64encode(comp.flush()).decode('ascii')


def generate_ssh_key():
    path = '/root/.ssh/temp'
    if os.getenv('HOME'):
        path = os.getenv('HOME') + '/.ssh/temp'
    os.makedirs(os.path.dirname(path), mode=0o700, exist_ok=True)
    subprocess.check_call(['ssh-keygen', '-t', 'ed25519', '-f', path, '-C', '', '-N', ''])
    return path


def generate_ssh_config(inventory, private_key_path):
    netglob = re.sub(r'\.0/24$', '.*', inventory['network'])
    return f'''
Host {netglob}
    User root
    IdentityFile {private_key_path}
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
'''


def main():
    ci_job_id = os.getenv('CI_JOB_ID')
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--url', metavar='URL', default='http://127.0.0.1:4949',
        help='URL of the vmine API server')
    parser.add_argument(
        '--ssh', metavar='USER@HOST',
        help='proxy the vmine API request through SSH')
    parser.add_argument(
        '--state-file', metavar='FILE',
        default=f'.vmine_group-{ci_job_id}' if ci_job_id else '.vmine_group',
        help='state file to store the vmine group ID')
    parser.add_argument(
        '--inventory', metavar='FILE', default='hosts.yml',
        help='float host inventory')
    parser.add_argument(
        '--image', metavar='NAME',
        help='base image to use for the VMs')
    parser.add_argument(
        '--ram', type=int,
        help='memory reservation for the VMs')
    parser.add_argument(
        '--cpu', type=int, default=1,
        help='CPU reservation for the VMs')
    parser.add_argument(
        '--ttl', metavar='DURATION', default='1h',
        help='TTL for the virtual machines')
    parser.add_argument(
        '--env', metavar='FILE',
        help='generate a dotenv file (for Gitlab CI)')
    parser.add_argument(
        '--dashboard-url', metavar='URL',
        help='vmine dashboard base URL (for Gitlab CI)')
    parser.add_argument(
        '--ssh-key', metavar='FILE',
        help='root SSH key to install on VMs')
    parser.add_argument(
        '--ssh-config', metavar='FILE',
        default='/root/.ssh/config',
        help='append SSH config to this file')
    parser.add_argument(
        '--name', metavar='NAME',
        help='group name (for named groups)')
    parser.add_argument(
        'cmd',
        choices=['up', 'down'])
    args = parser.parse_args()

    if args.cmd == 'up':
        host_attrs = {}
        if args.ram:
            host_attrs['ram'] = args.ram
        if args.cpu:
            host_attrs['cpu'] = args.cpu
        if args.image:
            host_attrs['image'] = args.image

        req = parse_inventory(args.inventory, host_attrs)
        req['ttl'] = args.ttl
        if args.name:
            req['name'] = args.name
        if args.ssh_key:
            ssh_key_path = args.ssh_key
        else:
            ssh_key_path = generate_ssh_key()
        with open(ssh_key_path + '.pub', 'r') as fd:
            req['ssh_key'] = fd.read().strip()

        os.umask(0o077)

        print(f'creating VM group with attrs {host_attrs} ...')
        print(f'vmine request: {req}')
        resp = do_request(args.url + '/api/create-group', args.ssh, req)
        group_id = resp['group_id']
        with open(args.state_file, 'w') as fd:
            fd.write(group_id)
        print(f'created VM group {group_id}')

        if args.ssh_config:
            print(f'updating ssh config')
            with open(args.ssh_config, 'a') as fd:
                fd.write(generate_ssh_config(req, ssh_key_path))

        if args.env:
            with open(args.env, 'w') as fd:
                fd.write(f'VMINE_ID={group_id}\n')
                if args.dashboard_url:
                    base_url = args.dashboard_url.rstrip('/')
                    payload = encode_dashboard_request(req)
                    dashboard_url = f'{base_url}/dash/{payload}'
                    fd.write(f'VMINE_GROUP_URL={dashboard_url}\n')
                    print(f'dashboard URL: {dashboard_url}')

    elif args.cmd == 'down':
        req = {}
        if args.name:
            req['name'] = args.name
            print(f'stopping VM group {args.name}...')
        else:
            try:
                with open(args.state_file) as fd:
                    group_id = fd.read().strip()
            except FileNotFoundError:
                print('state file not found, exiting')
                return
            req['group_id'] = group_id
            print(f'stopping VM group {group_id}...')
        try:
            do_request(args.url + '/api/stop-group', args.ssh, req)
        except:
            pass
        if args.state_file and os.path.exists(args.state_file):
            os.remove(args.state_file)


if __name__ == '__main__':
    main()
